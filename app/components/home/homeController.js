angular
    .module('app.home', [])
    .controller('HomeController', homeController);

homeController.$inject = ['$scope', '$http'];

function homeController($scope, $http) {
    $scope.testVar = 'hello';
    console.log($scope.testVar);
}
