# AngularScaffold
Scaffold for angular projects. Follows the practices mentioned in this https://scotch.io/tutorials/angularjs-best-practices-directory-structure.

## Installation & Run

Clone the repository:
`git clone https://github.com/WenInCode/AngularScaffold.git` or `https://bitbucket.org/twendlan/angularscaffold`

Install dependencies:

`npm install && bower install`

Start the server:

`npm start`
